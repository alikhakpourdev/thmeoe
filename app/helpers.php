<?php

function get_template_app_directory( $path = '' ) {

	$path_app = THEME_DIR . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;

	if ( strlen( $path ) > 0 ) {
		$path      = str_replace( '/', DIRECTORY_SEPARATOR, $path );
		$path_app = $path_app . $path;
	}

	return $path_app;
}


