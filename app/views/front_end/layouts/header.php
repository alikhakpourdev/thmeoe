<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once View::render('front_end/layouts/head.php'); ?>
</head>
<body>
<div class="trending-bar trending-light d-md-block">
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-md-9 text-center text-md-left">
				<p class="trending-title"><i class="tsicon fa fa-bolt"></i> Trending Now</p>
				<div id="trending-slide" class="owl-carousel owl-theme trending-slide">
					<div class="item">
						<div class="post-content">
							<h2 class="post-title title-small">
								<a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
							</h2>
						</div><!-- Post content end -->
					</div><!-- Item 1 end -->
					<div class="item">
						<div class="post-content">
							<h2 class="post-title title-small">
								<a href="#">Soaring through Southern Patagonia with the Premium Byrd drone</a>
							</h2>
						</div><!-- Post content end -->
					</div><!-- Item 2 end -->
					<div class="item">
						<div class="post-content">
							<h2 class="post-title title-small">
								<a href="#">Super Tario Run isn&rsquo;t groundbreaking, but it has Mintendo charm</a>
							</h2>
						</div><!-- Post content end -->
					</div><!-- Item 3 end -->
				</div><!-- Carousel end -->
			</div><!-- Col end -->
			<div class="col-md-3 text-md-right text-center">
				<div class="ts-date">
					<i class="fa fa-calendar-check-o"></i>May 29, 2017
				</div>
			</div><!-- Col end -->
		</div><!--/ Row end -->
	</div><!--/ Container end -->
</div><!--/ Trending end -->

<!-- Header start -->
<header id="header" class="header">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-3 col-sm-12">
				<div class="logo">
					<a href="index.html">
						<img src="/wp-content/themes/themeoe/assets/images/logos-logo.png" alt="">
					</a>
				</div>
			</div><!-- logo col end -->

			<div class="col-md-8 col-sm-12 header-right">
				<div class="ad-banner float-right">
					<a href="#">
						<img src="/wp-content/themes/themeoe/assets/images/banner-image-image1.png" class="img-fluid" alt="">
					</a>
				</div>
			</div><!-- header right end -->
		</div><!-- Row end -->
	</div><!-- Logo and banner area end -->
</header><!--/ Header end -->

<?php include_once View::render('front_end/layouts/navigation.php'); ?>
