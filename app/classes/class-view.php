<?php


class View {

	/**
	 * include view files from theme path
	 *
	 * @param string $view_path
	 * @param bool $views_dir
	 *
	 * @return null
	 */
	public static function render($view_path, $views_dir = true ) {
		//$path example return : project_path/wp-content/themes/themeoe/app/views/$view_path
		$path = $views_dir ? self::get_view_path( $view_path ) : $view_path;

		//check path file exists in themeoe/app/views/$view_name.php
		if ( file_exists( $path ) ) {
			// include file from $path
			include $path;
		}

		return null;
		//return view not found :)";
	}

	private static function get_view_path( $path = '' ) {

		$path_view = get_template_app_directory() . 'views' . DIRECTORY_SEPARATOR;

		if ( strlen( $path ) > 0 ) {
			$path      = str_replace( '/', DIRECTORY_SEPARATOR, $path );
			$path_view = $path_view . $path;
		}

		return $path_view;
	}
}