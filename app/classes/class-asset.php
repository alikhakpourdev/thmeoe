<?php


class Asset {

	protected static $Css = [
		"css/css-bootstrap.min.css",
		"css/css-iconfonts.css",
		"css/css-font-awesome.min.css",
		"css/css-owl.carousel.min.css",
		"css/css-owl.theme.default.min.css",
		"css/css-magnific-popup.css",
		"css/css-animate.css",
		"css/css-style.css",
		"css/css-responsive.css",
		"css/css-colorbox.css"
	];

	protected static $Js = [
		"js/7392-js-jquery.js",
		"js/5124-js-popper.min.js",
		"js/7155-js-bootstrap.min.js",
		"js/1898-js-jquery.magnific-popup.min.js",
		"js/7438-js-owl.carousel.min.js",
		"js/1585-js-jquery.colorbox.js",
		"js/287-js-custom.js"
	];

	public static function __callStatic( $name, $arguments ) {
		switch ( $name ):
			case 'css':

				if ( sizeof( $arguments ) > 0 ) {
					return THEME_URL . '/assets/css/' . $arguments[0];
				}
				echo self::theme_css();
				break;

			case 'js':
				if ( sizeof( $arguments ) > 0 ) {
					return THEME_URL . '/assets/js/' . $arguments[0];
				}

				echo self::theme_js();
				break;

			case 'image':
				return THEME_URL . '/assets/images/' . $arguments[0];
				break;

			default:
				return null;
				break;
		endswitch;

	}

	protected static function theme_css() {
		$css = '';

		foreach ( self::$Css as $path ) {
			$css .= "<link rel='stylesheet' href='" . THEME_URL . "/assets/{$path}'>";
		}

		return $css;
	}

	public static function theme_js() {
		$js = '';
		foreach ( self::$Js as $path ) {
			$js .= "<script src='" . THEME_URL . "/assets/{$path}' type='application/javascript'></script>";
		}

		return $js;
	}

}
