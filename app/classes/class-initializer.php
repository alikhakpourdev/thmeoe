<?php


class Initializer {

	public static function setup(  ) {
		//supports
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );

		//custom supports
		add_image_size( 'custom-image', 100, 100 );

		//filters
		add_filter( 'show_admin_bar', '__return_false' );

	}
}