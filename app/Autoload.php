<?php


class Autoload {

	/**
	 * call autoloader method from this class
	 * Autoload constructor.
	 */
	public function __construct() {
		try {
			//register autoloader
			spl_autoload_register( [ $this, 'autoloader' ] );

		} catch ( Exception $e ) {
			//return exception
			echo "can't load classes";
			die;
		}
	}

	/**
	 * includes files
	 *
	 * @param $class_name
	 */
	public function autoloader( $class_name ) {
		//get file path from class name
		$file = $this->convert_class_name_to_file_path( $class_name );

		//include file from classes directory
		try {
			//check file exists in directory
			if ( is_file( $file ) && file_exists( $file ) ) {
				include_once $file;
			}

		} catch ( Exception $e ) {
			//return exception
			echo "can't load this file : " . $class_name . ' in theme directory';
			die;
		}

	}

	/**
	 * get class file path from class name
	 *
	 * @param $class_name
	 *
	 * @return string
	 */
	private function convert_class_name_to_file_path( $class_name ) {
		//file name example : convert Asset to class-asset.php
		$file_name = 'class-' . strtolower( $class_name ) . '.php';

		// return class file path example : /wp-content/themes/themeoe/app/classes/class-asset.php
		return get_template_app_directory( "classes/{$file_name}" );
	}
}

new Autoload();