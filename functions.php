<?php

define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URL', get_template_directory_uri() );

require_once THEME_DIR . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'helpers.php';
require_once get_template_app_directory( 'Autoload.php' );

add_action( 'after_setup_theme', 'Initializer::setup');
